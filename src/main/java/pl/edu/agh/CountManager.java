package pl.edu.agh;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danielu221 on 09.04.16.
 */
public class CountManager extends Employee implements Manager {

    private int limit;
    private List<Employee> employeeList;

    public CountManager(String name, int salary,int limit){
        super(name, salary);
        this.limit=limit;
        this.employeeList=new ArrayList<Employee>();
    }

    int numberOfEmployees(){
        int number=0;
        for (Employee emp : employeeList)
            number++;
        return number;
    }
    //Jak można zrobić to ładniej, tzn ta sama metoda powtarza się w Budget Manager?
    public void hireEmployee(Employee n){
        if(canHire(n))
            employeeList.add(n);
}

    public boolean canHire(Employee n){
        if(numberOfEmployees()<limit)
            return true;
        else
            return false;
    }

    public List<Employee> getEmployeeList(){
        return employeeList;
    }

    @Override
    public String toString() {
        String printTeam=name+"-Manager\n";
        for(Employee emp :employeeList){
            printTeam+="\t\t"+emp.toString();
        }
        return printTeam;
    }
}
