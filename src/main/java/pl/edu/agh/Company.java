package pl.edu.agh;

/**
 * Created by danielu221 on 09.04.16.
 */
public class Company {
    private static Company company = new Company( );
    private CEO companyCEO;
    // A private Company prevents any other
    // class from instantiating.
       private Company(){ }

    // Static 'instance' method
    public static Company getInstance( ) {
        return company;
    }

    public String getCEOName() {
        return companyCEO.getName();
    }

    public void setCEO(CEO companyCEO){
        this.companyCEO=companyCEO;
    }

    @Override
    public String toString() {
        return companyCEO.toString();
    }


}
