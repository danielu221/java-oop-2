package pl.edu.agh;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danielu221 on 09.04.16.
 */
public class CEO extends Employee implements Manager {

    private List<Employee> managerList;
    private List<Employee> employeeList;

    public CEO(String name,int salary)
    {
        super(name,salary);
        this.managerList=new ArrayList<Employee>();
        this.employeeList=new ArrayList<Employee>();
    }

    public List<Employee> getEmployeeList(){
        return employeeList;
    }

    public List<Employee> getManagerList(){
        return managerList;
    }

    public void hireEmployee(Employee n){
        if(canHire(n))
            employeeList.add(n);
    }

    public void hireManager(Employee n){
        if(canHire(n))
            managerList.add(n);
    }

    public boolean canHire(Employee n){
        return true;
    }


    @Override
    public String toString() {
        String printTeam=name+"-CEO\n";
        for(Employee emp :managerList){
            printTeam+="\t"+emp.toString();
        }
        for(Employee emp :employeeList){
            printTeam+="\t"+emp.toString();
        }
        return printTeam;
    }
}
