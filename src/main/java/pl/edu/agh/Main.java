package pl.edu.agh;

/**
 * Created by danielu221 on 09.04.16.
 */
public class Main {
    public static void main(String[] args) {
        Company company;
        company=Company.getInstance();

        CEO companyCEO=new CEO ("Zbyszek",100000);
        company.setCEO(companyCEO);

        Employee employeeLeszek=new Employee("Leszek",3000);
        Employee employeeWacek=new Employee("Wacek",11000);
        Employee employeeJarek=new Employee("Jarek",12020);

        BudgetManager managerMaciek=new BudgetManager("Maciek",20200,90000);
        managerMaciek.hireEmployee(employeeLeszek);
        managerMaciek.hireEmployee(employeeWacek);

        Employee employeeFrodo=new Employee("Wacek",11000);

        CountManager managerKrzysiek=new CountManager("Krzysiek",20200,2);
        managerKrzysiek.hireEmployee(employeeFrodo);

        companyCEO.hireManager(managerKrzysiek);
        companyCEO.hireManager(managerMaciek);
        companyCEO.hireEmployee(employeeJarek);

        System.out.print(company.toString());


    }
}
