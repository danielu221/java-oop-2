package pl.edu.agh;


public class Employee {
    protected final String name;
    protected int salary;
    protected boolean satisfied;
    protected static int satisfiedSalary=10000;

    public Employee(String name,int salary){
        this.name=name;
        this.salary=salary;
    }

    public String getName(){
        return name;
    }

    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary=salary;
    }

    public boolean isSatisfied(){
        if(salary>satisfiedSalary)
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return name + "-Employee \n";
    }

}
