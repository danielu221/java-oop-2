package pl.edu.agh;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danielu221 on 09.04.16.
 */
public interface Manager  {

    List<Employee> getEmployeeList();
    public void hireEmployee(Employee n);
    public boolean canHire(Employee n);

}
