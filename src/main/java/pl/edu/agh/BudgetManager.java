package pl.edu.agh;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danielu221 on 09.04.16.
 */
public class BudgetManager extends Employee implements Manager{

    private int budget;
    private List<Employee> employeeList;

    public BudgetManager(String name, int salary,int budget){
        super(name, salary);
        this.budget=budget;
        this.employeeList=new ArrayList<Employee>();
    }

    protected int sumEmployeesSalary(){
        int sum=0;
        for(Employee emp : employeeList)
            sum+=emp.getSalary();
        return sum;
    }


    public boolean canHire(Employee candidate){
        if((candidate.getSalary()+sumEmployeesSalary())>budget)
            return false;
        else
            return true;
    }

    public void hireEmployee(Employee candidate) {
        if(canHire(candidate))
            employeeList.add(candidate);
    }

    public List<Employee> getEmployeeList(){
        return employeeList;
    }

    @Override
    public String toString() {
        String printTeam=name+"-Manager\n";
        for(Employee emp :employeeList){
            printTeam+="\t\t"+emp.toString();
        }
        return printTeam;
    }
}
