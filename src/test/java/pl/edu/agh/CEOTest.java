package pl.edu.agh;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by danielu221 on 10.04.16.
 */
public class CEOTest {

    CEO companyCEO;
    BudgetManager managerB;
    CountManager managerC;
    Employee employee;

    @Before
    public void setUp()throws Exception{
        companyCEO=new CEO("Maciej",200000);
        managerB=new BudgetManager("Krzystof",20000,100000);
        managerC=new CountManager("Radek",20000,1);
        employee=new Employee("Bartek",20000);

    }

    @Test
    public void testCanHire(){
        assertTrue(companyCEO.canHire(employee));
        assertTrue(companyCEO.canHire(managerB));
        assertTrue(companyCEO.canHire(managerC));
    }

    @Test
    public void testHireEmployee(){
        assertTrue(companyCEO.canHire(employee));
        assertTrue(companyCEO.canHire(managerB));
        assertTrue(companyCEO.canHire(managerC));
    }

    @Test
    public void testGetEmployeeList(){
        assertEquals(0,companyCEO.getEmployeeList().size());
        companyCEO.hireEmployee(employee);
        assertEquals(1,companyCEO.getEmployeeList().size());

        assertEquals(0,companyCEO.getManagerList().size());
        companyCEO.hireEmployee(managerB);
        assertEquals(1,companyCEO.getManagerList().size());

    }
}
