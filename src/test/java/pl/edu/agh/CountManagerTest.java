package pl.edu.agh;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by danielu221 on 10.04.16.
 */
public class CountManagerTest {

        CountManager manager;
        Employee employee1;
        Employee employee2;
        Employee employee3;

        @Before
        public void setUp()throws Exception{
            manager=new CountManager("Maciej",20000,1);
            employee1=new Employee("Jacek",10000);
            employee2=new Employee("Krzysztof",6000);
            employee3=new Employee("Michał",90000);
        }

        @Test
        public void testCanHire(){
            assertTrue(manager.canHire(employee1));
            manager.hireEmployee(employee2);
            assertFalse(manager.canHire(employee1));
        }

        @Test
        public void testNumberOfEmployees(){
            assertEquals(0,manager.numberOfEmployees());
            manager.hireEmployee(employee2);
            assertEquals(1,manager.numberOfEmployees());
        }

        @Test
        public void testHireEmployee(){
            manager.hireEmployee(employee1);
            manager.hireEmployee(employee2);
            assertEquals(true,manager.getEmployeeList().contains(employee1));
            assertEquals(false,manager.getEmployeeList().contains(employee2));
            assertEquals(false,manager.getEmployeeList().contains(employee3));
        }

        @Test
        public void testGetEmployeeList(){
            assertEquals(0,manager.getEmployeeList().size());
            manager.hireEmployee(employee1);
            assertEquals(1,manager.getEmployeeList().size());
            manager.hireEmployee(employee2);
            assertEquals(1,manager.getEmployeeList().size());

        }



}
