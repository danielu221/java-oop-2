package pl.edu.agh;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.*;
/**
 * Created by danielu221 on 09.04.16.
 */
public class BudgetManagerTest {

    BudgetManager manager;
    Employee employee1;
    Employee employee2;
    Employee employee3;

    @Before
    public void setUp()throws Exception{
        manager=new BudgetManager("Maciej",20000,18000);
        employee1=new Employee("Jacek",10000);
        employee2=new Employee("Krzysztof",6000);
        employee3=new Employee("Michał",90000);
    }

    @Test
    public void testCanHire(){
        assertTrue(manager.canHire(employee1));
        assertTrue(manager.canHire(employee2));
        assertFalse(manager.canHire(employee3));
    }

    @Test
    public void testSumEmployeesSalary(){
        assertEquals(0,manager.sumEmployeesSalary());
        manager.hireEmployee(employee1);
        assertEquals(10000,manager.sumEmployeesSalary());
        manager.hireEmployee(employee1);
        assertEquals(16000,manager.sumEmployeesSalary());
    }

    @Test
    public void testHireEmployee(){
        manager.hireEmployee(employee1);
        manager.hireEmployee(employee2);
        assertEquals(true,manager.getEmployeeList().contains(employee1));
        assertEquals(true,manager.getEmployeeList().contains(employee2));
        assertEquals(false,manager.getEmployeeList().contains(employee3));
    }

    @Test
    public void testGetEmployeeList(){
        assertEquals(0,manager.getEmployeeList().size());
        manager.hireEmployee(employee1);
        assertEquals(1,manager.getEmployeeList().size());
        manager.hireEmployee(employee2);
        assertEquals(2,manager.getEmployeeList().size());
        manager.hireEmployee(employee3);
        assertEquals(2,manager.getEmployeeList().size());
    }

}
