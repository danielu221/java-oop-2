package pl.edu.agh;

import org.junit.Test;
import org.junit.Before;
import static junit.framework.Assert.*;



/**
 * Created by danielu221 on 09.04.16.
 */
public class EmployeeTest {

    Employee employee;

    @Before
    public void setUp()throws Exception{
        employee=new Employee("Name",9000);
    }

    @Test
    public void employeeTest(){
        assertEquals("Name", employee.getName());
        assertEquals(9000, employee.getSalary());
        assertEquals(false, employee.isSatisfied());

        employee.setSalary(9999999);

        assertEquals(true, employee.isSatisfied());
    }

}
