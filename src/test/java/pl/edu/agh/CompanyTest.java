package pl.edu.agh;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by danielu221 on 10.04.16.
 */
public class CompanyTest {

    CEO companyCEO;
    Company company;
    BudgetManager managerB;
    CountManager managerC;
    Employee employeeManagerB;
    Employee employeeManagerC1;
    Employee employeeManagerC2;
    Employee employeeWithoutManager;
    @Before
    public void setUp()throws Exception{
        company=Company.getInstance();
        companyCEO=new CEO("Zbyszek",200000);
        managerB=new BudgetManager("Krzysiek",20000,100000);
        employeeManagerB=new Employee("Wacek",20000);
        managerC=new CountManager("Maciek",20000,3);
        employeeManagerC1=new Employee("Leszek",20000);
        employeeManagerC2=new Employee("Wacek",20000);
        employeeWithoutManager=new Employee("Jarek",20000);

        company.setCEO(companyCEO);
        companyCEO.hireManager(managerB);
        managerB.hireEmployee(employeeManagerB);
        companyCEO.hireManager(managerC);
        managerC.hireEmployee(employeeManagerC1);
        managerC.hireEmployee(employeeManagerC2);
        companyCEO.hireEmployee(employeeWithoutManager);
    }

    @Test
    public void getInstance(){
        assertEquals(company,Company.getInstance());
    }

    @Test
    public void getCEOName(){
        company.setCEO(companyCEO);
        assertEquals("Zbyszek",company.getCEOName());
    }

    @Test
    public void testToString(){
        String expected="Zbyszek-CEO\n" +
                "\tKrzysiek-Manager\n" +
                "\t\tWacek-Employee \n" +
                "\tMaciek-Manager\n" +
                "\t\tLeszek-Employee \n" +
                "\t\tWacek-Employee \n" +
                "\tJarek-Employee \n";
        assertEquals(expected,company.toString());
    }

}
